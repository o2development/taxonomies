<?php namespace O2Development\Taxonomies\Traits;

/**
 * Class TaxableTrait
 * @deprecated Use HasTaxonomies instead.
 * @package O2Development\Taxonomies\Traits
 */
trait TaxableTrait
{
     use HasTaxonomies;
}